use std::str::FromStr;

use trust_dns_proto::rr::dnssec::rdata::{DNSSECRData, DNSKEY, DS};
use trust_dns_proto::rr::dnssec::DigestType;
use trust_dns_proto::rr::domain::Name;
use trust_dns_proto::rr::{RData, RecordType};
use trust_dns_resolver::config::ResolverConfig;
use trust_dns_resolver::error::ResolveResult;
use trust_dns_resolver::system_conf::read_system_conf;
use trust_dns_resolver::Resolver;

fn collect_dnskeys(resolver: &Resolver, name: &str) -> ResolveResult<Vec<DNSKEY>> {
    let mut result = Vec::new();
    let response = resolver.lookup(name, RecordType::DNSKEY)?;
    for resp in response.iter() {
        match resp {
            RData::DNSSEC(DNSSECRData::DNSKEY(x)) => {
                if x.secure_entry_point() {
                    result.push((*x).clone())
                }
            }
            _ => (),
        };
    }
    Ok(result)
}

fn collect_dses(resolver: &Resolver, name: &str) -> ResolveResult<Vec<DS>> {
    let mut result = Vec::new();
    let response = resolver.lookup(name, RecordType::DS)?;
    for resp in response.iter() {
        match resp {
            RData::DNSSEC(DNSSECRData::DS(x)) => result.push((*x).clone()),
            _ => (),
        };
    }
    Ok(result)
}

pub fn synchronized(name: &str) {
    let (_config, options) = read_system_conf().unwrap();
    // options.validate = true;
    // options.preserve_intermediates = true;
    let resolver = Resolver::new(ResolverConfig::cloudflare(), options).unwrap();
    let dses = match collect_dses(&resolver, name) {
        Ok(vec) => vec,
        _ => {
            println!("INFO Failure resolving any DS records");
            return;
        }
    };
    let dnskeys = match collect_dnskeys(&resolver, name) {
        Ok(vec) => vec,
        _ => {
            println!("ERROR Failure resolving DNSKEY records (but found DS records)");
            return;
        }
    };

    for ds in &dses {
        if !dnskeys.iter().any(|dnskey| {
            let dgst = dnskey
                .to_digest(&Name::from_str(name).unwrap(), DigestType::SHA256)
                .unwrap();
            dgst.as_ref().to_vec() == ds.digest()
        }) {
            println!(
                "INFO {} has DS with keytag {} but no matching DNSKEY",
                name,
                ds.key_tag()
            );
        }
    }

    for dnskey in &dnskeys {
        let dgstobj = dnskey
            .to_digest(&Name::from_str(name).unwrap(), DigestType::SHA256)
            .unwrap();
        let dgst = dgstobj.as_ref().to_vec();

        if !dses.iter().any(|ds| dgst == ds.digest()) {
            println!(
                "ERROR {} has DNSKEY with keytag {} but no matching DS",
                name,
                dnskey.calculate_key_tag().unwrap()
            );
        }
    }
}
