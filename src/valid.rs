use chrono::{TimeZone, Utc};

use trust_dns_proto::rr::dnssec::rdata::DNSSECRData;
use trust_dns_proto::rr::{RData, RecordType};
use trust_dns_resolver::config::{NameServerConfigGroup, ResolverConfig};
use trust_dns_resolver::system_conf::read_system_conf;
use trust_dns_resolver::Resolver;

pub fn valid(name: &str) -> Result<(),std::io::Error> {
    let (_config, mut options) = read_system_conf().unwrap();
    options.edns0 = true;
    let resolver = Resolver::new(
        ResolverConfig::from_parts(
            None,
            vec![],
            NameServerConfigGroup::from_ips_clear(
                &["2001:4b98:dc2:47:216:3eff:fea2:1343".parse().unwrap()],
                53,
                true,
            ),
        ),
        options,
    )?;
    //let resolver = Resolver::new(config,ResolverOpts{/*validate: true, preserve_intermediates: true,*/ ..options}).unwrap();

    let rrsigs = resolver.lookup(name, RecordType::RRSIG)?;
    let now = Utc::now();

    for rtype in &[RecordType::SOA, RecordType::NS, RecordType::DNSKEY] {
        let _ = resolver.lookup(name, *rtype)?;
        let expirations: Vec<u32> = rrsigs
            .iter()
            .map(|rrsig| match rrsig {
                RData::DNSSEC(DNSSECRData::SIG(x)) => {
                    if x.type_covered() == *rtype {
                        Some(x.sig_expiration())
                    } else {
                        None
                    }
                }
                _ => None,
            })
            .filter_map(|opt| opt)
            .collect();

        match expirations.iter().max() {
            None => println!(
                "ERROR did not find matching RRSIG records for type {:?}",
                rtype
            ),
            Some(latest) => {
                let then = Utc.timestamp((*latest).into(), 0);
                let duration = then - now;

                if duration.num_days() < 5 {
                    println!("WARN {} has records expiring on {}", name, then);
                }
            }
        };
    }
    Ok(())
}
