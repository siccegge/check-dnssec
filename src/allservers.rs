use std::net::{IpAddr, SocketAddr};

use itertools::Itertools;
use trust_dns_resolver::config::{
    LookupIpStrategy, NameServerConfig, Protocol, ResolverConfig, ResolverOpts,
};
use trust_dns_resolver::error::ResolveResult;
use trust_dns_resolver::lookup::SoaLookup;
use trust_dns_resolver::lookup_ip::LookupIp;
use trust_dns_resolver::system_conf::read_system_conf;
use trust_dns_resolver::Resolver;

fn get_addresses(resolver: &Resolver, name: &str) -> ResolveResult<LookupIp> {
    let response = resolver.lookup_ip(name)?;
    Ok(response)
}

fn get_soa(options: &ResolverOpts, name: &str, addr: &IpAddr) -> ResolveResult<SoaLookup> {
    let mut config = ResolverConfig::new();
    config.add_name_server(NameServerConfig {
        socket_addr: SocketAddr::new(*addr, 53),
        protocol: Protocol::Tcp,
        tls_dns_name: None,
        bind_addr: None,
        trust_nx_responses: true,
    });
    let resolver = Resolver::new(config, *options)?;
    let response = resolver.soa_lookup(name)?;
    Ok(response)
}

pub fn allservers(name: &str) {
    let fun = || -> ResolveResult<()> {
        let (config, mut options) = read_system_conf()?;
        options.ip_strategy = LookupIpStrategy::Ipv4AndIpv6;
        let resolver = Resolver::new(config, options)?;
        let response = resolver.ns_lookup(name)?;
        let nameservers: Vec<String> = response.iter().map(|resp| resp.to_utf8()).collect();
        let ips = nameservers
            .iter()
            .map(|n| get_addresses(&resolver, n))
            .flatten_ok()
            .collect::<ResolveResult<Vec<IpAddr>>>()?;

        let soas: Vec<ResolveResult<SoaLookup>> =
            ips.iter().map(|ip| get_soa(&options, name, ip)).collect();
        let zip = ips.iter().zip(soas.iter());

        for (ip, soa) in zip.clone() {
            match soa {
                Err(err) => println!("ERROR: {}: {}", ip, err),
                Ok(_) => (),
            }
        }

        let serials: Vec<u32> = soas
            .iter()
            .filter_map(|soa| match soa {
                Ok(soa) => Some(soa.iter()),
                Err(_) => None,
            })
            .flatten()
            .map(|soa| soa.serial())
            .collect();

        if !serials.windows(2).all(|s| s[0] == s[1]) {
            println!("WARN divergent zone files");
            for (ip, soa) in zip {
                match soa {
                    Err(_) => (),
                    Ok(soa) => println!("{} -> {}", soa.iter().next().unwrap().serial(), ip),
                }
            }
        }
        Ok(())
    };
    fun().unwrap();
}
