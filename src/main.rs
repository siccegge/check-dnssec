use std::env;
mod allservers;
mod synchronized;
mod valid;

fn main() {
    let arg = env::args().nth(1);
    match arg {
        Some(name) => {
            synchronized::synchronized(&name);
            allservers::allservers(&name);
            valid::valid(&name).expect("ERROR something went wrong with validating RRSIG expiration");
        }
        None => panic!("No Name provided"),
    }
}
